# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : pocviews.py

from rest_framework.decorators import api_view
from django.http import JsonResponse
from Base.Functions.basefun import xssfilter
from django.views.decorators.csrf import csrf_protect
from .. import models, forms
from .. import serializers
from ..Functions import pocfun
from Base.Functions import basefun
from ..Functions import pocloader


@api_view(['GET'])
def poc_check(request, poc_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = pocfun.check_poc_permission(poc_id, user)
        if item_get:
            key = item_get.key
            poc = item_get.poc
            res = pocloader.poc2file(poc, key, item_get.require)
            if res:
                item_get.is_check = True
                item_get.update_user = user
                item_get.save()
                data['code'] = 0
                data['msg'] = '审核完成'
            else:
                data['msg'] = 'POC生成或插件包安装失败，请联系管理员'
        else:
            data['msg'] = '选定poc不存在'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def poc_test(request, poc_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = pocfun.check_poc_permission(poc_id, user)
        if item_get:
            form = forms.PocTestForm(request.POST)
            if form.is_valid():
                host = form.cleaned_data['host']
                key = item_get.key
                poc = item_get.poc
                res_write = pocloader.poc2file(poc, key, item_get.require)
                if res_write:
                    res = pocloader.poc_check(key, host)
                    data['code'] = 0
                    data['msg'] = res
                else:
                    data['msg'] = 'POC生成或插件包安装失败，请联系管理员'
            else:
                data['msg'] = '请检查输入'
        else:
            data['msg'] = '选定poc不存在'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
def poc_delete(request, poc_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    item_get = pocfun.check_poc_permission(poc_id, user)
    if item_get:
        item_get.delete()
        data['code'] = 0
        data['msg'] = '删除成功'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
def poc_details(request, poc_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    item_get = pocfun.check_poc_permission(poc_id, user)
    if item_get:
        data_get = serializers.PocDetailsSerializer(instance=item_get)
        data['data'] = xssfilter(data_get.data)
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def poc_create(request):
    data = {
        "code": 1,
        "msg": "添加成功,请请等候审核",
        "data": []
    }
    user = request.user
    form = forms.PocUpdateForm(request.POST)
    if form.is_valid():
        name = form.cleaned_data['name']
        level = form.cleaned_data['level']
        key = basefun.getuuid(name)
        introduce = form.cleaned_data['introduce']
        description = form.cleaned_data['description']
        fix = form.cleaned_data['fix']
        poc = form.cleaned_data['poc']
        models.POC.objects.get_or_create(
            name=name,
            level=level,
            key=key,
            introduce=introduce,
            description=description,
            fix=fix,
            poc=poc,
            user=user,
        )
        data['code'] = 0
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def poc_update(request, poc_id):
    data = {
        "code": 1,
        "msg": "更新成功",
        "data": []
    }
    user = request.user
    poc_item = models.POC.objects.filter(id=poc_id).first()
    form = forms.PocUpdateForm(request.POST, instance=poc_item)
    if form.is_valid():
        form.save()
        poc_item.is_check = False
        poc_item.save()
        data['code'] = 0
        data['msg'] = '更新成功'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)
