# coding:utf-8
'''
Created on 2018年12月7日

@author: 残源
'''

from rest_framework.request import Request
from django.utils.functional import SimpleLazyObject
from django.contrib.auth.middleware import get_user
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.utils.deprecation import MiddlewareMixin


def get_user_jwt(request):
    user = get_user(request)
    if user.is_authenticated:
        return user
    try:
        user_jwt = JWTAuthentication().authenticate(Request(request))
        if user_jwt is not None:
            return user_jwt[0]
    except:
        pass
    return user


class AuthenticationMiddlewareJWT(MiddlewareMixin):
    def process_request(self, request):
        assert hasattr(request,'session')
        request.user = SimpleLazyObject(lambda: get_user_jwt(request))
