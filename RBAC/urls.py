# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from .views import views, roles

urlpatterns = [
    path('login/', views.login, name='login'),
    path('session/', views.session, name='session'),
    path('selfinfo/', views.selfinfo, name='selfinfo'),
    # path('updateinfo/', views.selfupdate, name='selfupdate'),

    path('menu/', views.get_menu, name='menu'),
    path('logout/', views.logout, name='logout'),
    path('resetpsd/', views.resetpsd, name='resetpsd'),

    path('list/roles/', roles.role_list, name='role_list'),

]