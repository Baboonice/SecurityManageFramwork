# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py
from rest_framework import serializers
from . import models


class BuildingProjectSerializer(serializers.ModelSerializer):
    createtime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Building_Projects
        fields = '__all__'


class BuildingProjectsLEVELSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Building_Projects_LEVEL
        fields = "__all__"


class BuildingPhishingSerializer(serializers.ModelSerializer):
    createtime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Building_Phishing
        fields = '__all__'


class BuildingPhishingVictimSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Building_Phishing_Victim
        fields = "__all__"


class BuildingEventSerializer(serializers.ModelSerializer):
    starttime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Building_Event
        fields = '__all__'


class BuildingEventLEVELSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Building_Event_LEVEL
        fields = "__all__"