# coding:utf-8
from rest_framework.decorators import api_view
from django.http import JsonResponse
from Base.Functions.basefun import MyPageNumberPagination, xssfilter, getuuid
from .. import models,forms
from .. import serializers
from datetime import datetime
from django.views.decorators.csrf import csrf_protect


# Create your views here.
@api_view(['GET'])
def mainlist(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    starttime = request.GET.get('starttime', '1999-1-1')
    endtime = request.GET.get('endtime', datetime.now())
    list_get = models.Building_Phishing.objects.filter(name__icontains=key,
                                         updatetime__gte=starttime,
                                         updatetime__lte=endtime).order_by('-updatetime')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.BuildingPhishingSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def phishing_create(request):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    form = forms.BuildingPhishingUpdateForm(request.POST)
    if form.is_valid():
        key_get = getuuid(form.cleaned_data['name'])
        models.Building_Phishing.objects.get_or_create(name=form.cleaned_data['name'],
                                              key=key_get,
                                              number=form.changed_data['number'],
                                              scope=form.changed_data['scope'],
                                              description=form.cleaned_data['description'])
        data['code'] = 0
        data['msg'] = '创建成功'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def phishing_update(request, phishing_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    phishing_get = models.Building_Phishing.objects.filter(id=phishing_id).first()
    if phishing_get:
        form = forms.BuildingPhishingUpdateForm(request.POST, instance=phishing_get)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = '创建成功'
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['GET'])
def phishing_delete(request, phishing_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    phishing_get = models.Building_Phishing.objects.filter(id=phishing_id).first()
    if phishing_get:
        phishing_get.delete()
        data['msg'] = '删除成功'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['GET'])
def phishing_lock(request, phishing_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    phishing_get = models.Building_Phishing.objects.filter(id=phishing_id).first()
    if phishing_get:
        phishing_get.is_lock = True
        phishing_get.save()
        data['msg'] = '锁定成功'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['GET'])
def victimlist(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.Building_Phishing.objects.all().order_by('-createtime')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.BuildingPhishingVictimSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)