# coding:utf-8
from .. import models
from .. import serializers
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import JsonResponse
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models
from django.db.models import Q
from .. import serializers
from datetime import datetime


# Create your views here.

@api_view(['GET'])
def Log_list(request):
    user = request.user
    log_list = models.Log.objects.filter(user=user).order_by('-create_time')[:5]
    log_serializer = serializers.LogSerializer(log_list, many=True)
    data = {
        "code": 0
        , "msg": ""
        , "data": log_serializer.data
    }
    return Response(data)


@api_view(['GET'])
def mainlist(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    key = request.GET.get('key', '')
    starttime = request.GET.get('starttime', '1999-1-1')
    endtime = request.GET.get('endtime', datetime.now())
    if user.is_superuser:
        list_get = models.Log.objects.filter(Q(type__icontains=key) |
                                             Q(action__icontains=key) |
                                             Q(user__username__icontains=key) |
                                             Q(status=key),
                                             create_time__gte=starttime,
                                             create_time__lte=endtime).order_by('-create_time')
    else:
        list_get = models.Log.objects.filter(Q(type__icontains=key) |
                                             Q(action__icontains=key) |
                                             Q(status=key),
                                             user=user,
                                             create_time__gte=starttime,
                                             create_time__lte=endtime).order_by('-create_time')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.LogAllSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)
