#!/bin/bash -e
python3.8 -m pip install -r requirements.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host=mirrors.aliyun.com
python3.8 manage.py makemigrations
python3.8 manage.py migrate
python3.8 initdata.py
python3.8 manage.py collectstatic --noinput
python3.8 manage.py createsuperuser
python3.8 manage.py runserver 0.0.0.0:8000 --insecure >> logs/runserver.log &
