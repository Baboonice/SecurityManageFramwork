# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from .views import groupviews, assetviews, portviews, pluginviews, vulnviews
from .views import typeviews, languageviews, baseinfoviews

urlpatterns = [
    path('group/list/', groupviews.list_views),
    path('group/asset/list/<str:group_id>/', groupviews.asset_list_views),
    path('group/delete/<str:group_id>/', groupviews.delete_views),
    path('group/create/', groupviews.create_views),
    path('group/update/<str:group_id>/',groupviews.update_views),

    path('asset/list/', assetviews.list_views),
    path('asset/details/<str:asset_id>/', assetviews.details_views),
    path('asset/select/', assetviews.select_asset_views),
    path('asset/delete/<str:asset_id>/', assetviews.delete_views),
    path('asset/create/', assetviews.create_views),
    path('asset/update/<str:asset_id>/',assetviews.update_views),

    path('port/list/<str:asset_id>/', portviews.list_views),
    path('port/delete/<str:port_id>/', portviews.delete_views),
    path('port/create/<str:asset_id>/', portviews.create_views),
    path('port/update/<str:port_id>/', portviews.update_views),

    path('plugin/list/<str:asset_id>/', pluginviews.list_views),
    path('plugin/delete/<str:plugin_id>/', pluginviews.delete_views),
    path('plugin/create/<str:asset_id>/', pluginviews.create_views),
    path('plugin/update/<str:plugin_id>/', pluginviews.update_views),

    path('vuln/list/<str:asset_id>/', vulnviews.list_views),

    path('baseinfo/update/<str:key>/<str:asset_id>/', baseinfoviews.update_views),

    # 资产类型下拉框
    path('type/select/', typeviews.select_type_views),
    path('language/select/', languageviews.select_language_views),
]
