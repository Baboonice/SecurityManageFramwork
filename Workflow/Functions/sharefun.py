# -*- coding:utf-8 -*-
# @Time: 2020/12/31
# @Author: 残源
# @Mail: canyuan@semf.top
from Base.Functions import basefun
from .. import models


def vuln2share(ticket_obj, user_obj):
    key = basefun.getuuid(ticket_obj.name)
    share_item = models.ShareKey.objects.create(
        key=key,
        description='',
        user=user_obj,
        ticket=ticket_obj
    )
    if share_item:
        ticket_obj.is_share = True
        ticket_obj.save()
        return key
    return False


def share_check(key):
    share_item = models.ShareKey.objects.filter(key=key, is_use=True).first()
    if share_item:
        return share_item
    return False
