# -*- coding: utf-8 -*-
# @Time    : 2020/10/13
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : workflowfun.py
from .. import models as workflowmodels
from . import logfun


def create_workflow(user_obj, fun_key, source_obj):
    workflow_get = workflowmodels.Workflow.objects.filter(key=fun_key).first()
    if workflow_get:
        ticket_get = workflowmodels.Tickets.objects.get_or_create(
            name=source_obj.name,
            ticket_key=fun_key,
            ticket_id=source_obj.id,
            workflow=workflow_get,
            user=user_obj
        )
        source_obj.status = workflow_get.status
        source_obj.save()
        data = {
            'status': True,
            'user': user_obj,
            'ticket': ticket_get[0],
            'description': '创建单据'
        }
        logfun.create_workflowlog(data)
        return ticket_get[0]
    return False


def delete_workflow(fun_key, source_id):
    ticket_get = workflowmodels.Tickets.objects.filter(ticket_key=fun_key, ticket_id=source_id).first()
    if ticket_get:
        ticket_get.delete()
        return True
    else:
        return False



