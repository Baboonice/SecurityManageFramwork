# -*- coding: utf-8 -*-
# @Time    : 2020/10/13
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py
from rest_framework import serializers
from . import models


class TicketsSerializer(serializers.ModelSerializer):
    ticket_name = serializers.SerializerMethodField()
    status = serializers.CharField(source='workflow.status')
    createtime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Tickets
        fields = ('id', 'name', 'ticket_name', 'status', 'createtime', 'updatetime')

    def get_ticket_name(self, obj):
        return '预留位置'


class WorkflowLogSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.profile.title')
    updatetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.WorkflowLog
        fields = "__all__"
