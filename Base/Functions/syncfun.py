# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : syncfun.py

from .. import models


def dic2dep(dep_dic):
    """
    :param dep_dic:{'name':'一级部门,二级部门,三级部门'}
    """
    dep_list = dep_dic.get('name').split(',')
    dep_root = models.Department.objects.none()
    for item in dep_list:
        dep_item = models.Department.objects.get_or_create(name=item)
        if dep_item != dep_root:
            dep_item.parent = dep_root
            dep_item.save()
        dep_root = dep_item[0]
    return True


def dic2person(person_dic):
    """
    :param person_dic:{'name':'xxx','mail':'xxxx@xxx.com','phone':'111111111','dep':'xxxx'}
    """
    person_item = models.Person.objects.get_or_create(name=person_dic.get('name'),
                                                      mail=person_dic.get('mail'),
                                                      )
    person_item = person_item[0]
    person_item.phone = person_dic.get('phone'),
    dep_name = person_dic.get('dep')
    dep_get = models.Department.objects.get_or_create(name=dep_name)
    if dep_get:
        person_item.dep = dep_get[0]
    person_item.save()


def dic2client(client_dic):
    """
     :param client_dic:{'ip':'xxx.xx.xx.xx', 'mac':'xx-xx-xx-xx-xx-xx', 'des': '备注说明', 'mail':'使用人邮箱'}
    """
    client_item = models.Client.objects.get_or_create(mac=client_dic.get('mac'))[0]
    client_item.ip = client_dic.get('ip')
    client_item.des = client_dic.get('des')
    person_item = models.Person.objects.filter(mail=client_dic.get['mail']).first()
    if person_item:
        client_item.person = person_item
    client_item.save()


