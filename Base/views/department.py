# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : department.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms
from .. import serializers
from django.views.decorators.csrf import csrf_protect


@api_view(['GET'])
def department_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        key = request.GET.get('key', '')
        list_get = models.Department.objects.filter(name__icontains=key).order_by('id')
        list_count = list_get.count()
        pg = MyPageNumberPagination()
        list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.DepartmentSerializer(instance=list_page, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)


@api_view(['GET'])
def department_delete(request, department_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = models.Department.objects.filter(id=department_id).first()
        if item_get:
            item_get.delete()
            data['code'] = 0
            data['msg'] = '操作成功'
        else:
            data['msg'] = '你要干啥'
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def department_create(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        form = forms.DepartmentForm(request.POST)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = '添加成功'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def department_update(request, department_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        department_get = models.Department.objects.filter(id=department_id).first()
        if department_get:
            form = forms.DepartmentForm(request.POST, instance=department_get)
            if form.is_valid():
                if department_get.parent:
                    department_get.parent = form.cleaned_data['parent']
                    department_get.name = form.cleaned_data['name']
                    data['code'] = 0
                    data['msg'] = '更新成功'
                else:
                    department_get.name = form.cleaned_data['name']
                    data['code'] = 0
                    data['msg'] = '更新成功，根部门父节点已忽略'
                department_get.save()
            else:
                data['msg'] = '请检查参数'
        else:
            data['msg'] = '指定参数不存在'
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)